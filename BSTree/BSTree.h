#pragma once
#include <iostream>
namespace my
{
	template<class K>
	struct BSTreeNode
	{
		BSTreeNode<K>* _left;
		BSTreeNode<K>* _right;
		K _key;
		BSTreeNode(const K& key = K())
			:_left(nullptr),
			_right(nullptr),
			_key(key)
		{}
	};

	template<class K>
	class BSTree
	{
		typedef BSTreeNode<K> Node;
	public:
		BSTree()
			:_root(nullptr)
		{}

		bool Insert(const K& key)
		{
			if (_root == nullptr)
			{
				_root = new Node(key);
				return true;
			}

			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key > key)
				{
					parent = cur;
					cur = parent->_left;
				}
				else if (cur->_key < key)
				{
					parent = cur;
					cur = parent->_right;
				}
				else
					return false;
			}

			cur = new Node(key);
			if (parent->_key > key)
				parent->_left = cur;
			else
				parent->_right = cur;

			return true;
		}

		bool Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key == key)
					return true;
				else if (cur->_key > key)
					cur = cur->_left;
				else
					cur = cur->_right;
			}
			return false;
		}

		bool Erase(const K& key)
		{
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key == key)
					break;
				else if (cur->_key > key)
				{
					parent = cur;
					cur = parent->_left;
				}
				else
				{
					parent = cur;
					cur = parent->_right;
				}
			}

			if (cur == nullptr)
				return false;
			else if (cur == _root)
			{
				if (cur->_right == nullptr)
					_root = cur->_left;
				else if (cur->_left == nullptr)
					_root = cur->_right;
				else
				{
					Node* rightMinParent = cur;
					Node* rightMin = cur->_right;
					while (rightMin->_left)
					{
						rightMinParent = rightMin;
						rightMin = rightMin->_left;
					}
					rightMinParent->_left = rightMin->_right;
					rightMin->_left = _root->_left;
					rightMin->_right = _root->_right;
					_root = rightMin;
				}
				delete cur;
			}
			else
			{
				if (cur->_right == nullptr)
				{
					if (parent->_left == cur)
						parent->_left = cur->_left;
					else
						parent->_right = cur->_left;
				}
				else if (cur->_left == nullptr)
				{
					if (parent->_left == cur)
						parent->_left = cur->_right;
					else
						parent->_right = cur->_right;
				}
				else
				{
					Node* rightMinParent = cur;
					Node* rightMin = cur->_right;
					while (rightMin->_left)
					{
						rightMinParent = rightMin;
						rightMin = rightMin->_left;
					}
					rightMinParent->_left = rightMin->_right;
					rightMin->_left = cur->_left;
					rightMin->_right = cur->_right;
					if (parent->_key > key)
					{
						parent->_left = rightMin;
					}
					else
					{
						parent->_right = rightMin;
					}
				}
				delete cur;
			}
			return true;
		}

		void InOrder()
		{
			_InOrder(_root);
			std::cout << std::endl;
		}
	private:
		void _InOrder(Node* root)
		{
			if (root == nullptr) return;
			_InOrder(root->_left);
			std::cout << root->_key << " ";
			_InOrder(root->_right);
		}

	private:
		Node* _root;
	};

	void BSTreeTest1()
	{
		int arr[] = { 24, 7, 2, 654, 35, 74, 24, 2345, 96, 1, 6, 77 };
		BSTree<int> bs;
		for (int e : arr)
		{
			bs.Insert(e);
		}
		bs.InOrder();
		std::cout << bs.Find(55) << std::endl;
		std::cout << bs.Erase(2345) << std::endl;
		bs.InOrder();
	}
}