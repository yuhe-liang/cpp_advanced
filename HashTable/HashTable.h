#pragma once
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

namespace my
{
	template<class K>
	struct HashFunc
	{
		size_t operator()(const K& key)
		{
			return (size_t)key;
		}
	};

	template<>
	struct HashFunc<std::string>
	{
		size_t operator()(const std::string& key)
		{
			size_t hash = 0;
			for (auto ch : key)
			{
				hash *= 131;
				hash += ch;
			}

			return hash;
		}
	};

	enum State
	{
		EMPTY,
		EXIST,
		DELETE
	};

	template<class K, class V>
	struct HashData
	{
		std::pair<K, V> _kv;
		State _state = EMPTY;
	};

	template <class K, class V, class Hash = HashFunc<K>>
	class HashTable
	{
	private:
		void swap(HashTable<K, V, Hash>& newHT)
		{
			std::swap(_v, newHT._v);
			std::swap(_size, newHT._size);
		}

		void CheckCapacity()
		{
			if (10 * _size / _v.size() >= 7)
			{
				HashTable<K, V, Hash> newHT(2 * _v.size());
				for (int i = 0; i < _v.size(); ++i)
				{
					if (_v[i]._state == EXIST)
						newHT.insert(_v[i]._kv);
				}
				swap(newHT);
			}
		}
	public:
		HashTable()
			:_size(0)
		{
			_v.resize(10);
		}

		HashTable(size_t size)
			:_size(0)
		{
			_v.resize(size);
		}

		HashData<K, V>* find(const K& key)
		{
			Hash hs;
			size_t hsKey = hs(key) % _v.size();
			while (_v[hsKey]._state != EMPTY)
			{			
				if (_v[hsKey]._state == EXIST && _v[hsKey]._kv.first == key)
					return &_v[hsKey];
				++hsKey;
				hsKey %= _v.size();
			}
			return nullptr;
		}

		bool insert(const std::pair<K, V>& kv)
		{
			if (find(kv.first))
				return false;

			CheckCapacity();

			Hash hs;
			size_t hsKey = hs(kv.first) % _v.size();

			while (_v[hsKey]._state == EXIST)
			{
				++hsKey;
				hsKey %= _v.size();
			}

			_v[hsKey]._kv = kv;
			_v[hsKey]._state = EXIST;
			++_size;
			return true;
		}

		bool erase(const K& key)
		{
			HashData<K, V>* ret = find(key);
			if (ret == nullptr)
				return false;
			else
			{
				ret->_state = DELETE;
				--_size;
			}
		}

		V& operator[](K& key)
		{
			insert(std::make_pair(key, V()));
			return find(key)->_kv.second;
		}

		void Print()
		{
			for (auto e : _v)
			{
				if (e._state == EXIST)
				{
					std::cout << e._kv.first << ":" << e._kv.second << std::endl;
				}
			}
		}
	private:
		std::vector<HashData<K, V>> _v;
		size_t _size;
	};

	void HashTest1()
	{
		int arr[] = { 123, 1232, 122, 123, 5, 76, 43, 5, 44, 234, 8678 };
		HashTable<int, int> hs;
		for (auto& e : arr)
		{
			hs.insert(std::make_pair(e, e));
		}
		hs.Print();
	}

	void HashTest2()
	{
		std::string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����",
	"ƻ��", "�㽶", "ƻ��", "�㽶","ƻ��","��ݮ", "ƻ��","��ݮ" };
		HashTable<std::string, int> hs;
		for (auto& e : arr)
		{
			hs[e]++;
		}
		hs.Print();
	}
}