#pragma once
#include <utility>
#include <iostream>
#include <vector>

namespace my
{
	enum Color
	{
		RED,
		BLACK
	};

	template<class K, class V>
	struct RBTreeNode
	{
		RBTreeNode<K, V>* _parent;
		RBTreeNode<K, V>* _left;
		RBTreeNode<K, V>* _right;
		std::pair<K, V> _kv;
		Color _color;

		RBTreeNode(const std::pair<K, V>& kv = std::pair<K, V>(), Color color = RED)
			:_parent(nullptr)
			,_left(nullptr)
			,_right(nullptr)
			,_kv(kv)
			,_color(color)
		{}
	};

	template<class K, class V>
	class RBTree
	{
		typedef RBTreeNode<K, V> Node;
	public:
		RBTree()
		{
			_head = new Node({ K(), V() });
		}

		bool insert(const std::pair<K, V>& kv)
		{
			Node* root = _head->_parent;
			// 插入
			if (root == nullptr)
			{
				root = new Node(kv);
				root->_parent = _head;
				_head->_parent = _head->_left = _head->_right = root;
				root->_color = BLACK;
				return true;
			}

			Node* parent = _head;
			Node* cur = root;
			while (cur)
			{
				if (cur->_kv.first > kv.first)
				{
					parent = cur;
					cur = parent->_left;
				}
				else if (cur->_kv.first < kv.first)
				{
					parent = cur;
					cur = parent->_right;
				}
				else
					return false;
			}

			cur = new Node(kv);
			if (parent->_kv.first > kv.first)
				parent->_left = cur;
			else
				parent->_right = cur;

			cur->_parent = parent;

			//调整树使其符合规则
			while (parent != _head && parent->_color == RED)
			{
				Node* gf = parent->_parent;
				if (parent == gf->_left)
				{
					Node* uc = gf->_right;
					if (uc && uc->_color == RED)
					{
						parent->_color = BLACK;
						uc->_color = BLACK;
						gf->_color = RED;
						cur = gf;
						parent = gf->_parent;
					}
					else
					{
						if (cur == parent->_left)
						{
							RotateR(gf);
							parent->_color = BLACK;
							gf->_color = RED;
						}
						else
						{
							RotateL(parent);
							RotateR(gf);
							cur->_color = BLACK;
							gf->_color = RED;
						}
						break;
					}
				}
				else
				{
					Node* uc = gf->_left;
					if (uc && uc->_color == RED)
					{
						parent->_color = BLACK;
						uc->_color = BLACK;
						gf->_color = RED;
						cur = gf;
						parent = gf->_parent;
					}
					else
					{
						if (cur == parent->_right)
						{
							RotateL(gf);
							parent->_color = BLACK;
							gf->_color = RED;
						}
						else
						{
							RotateR(parent);
							RotateL(gf);
							cur->_color = BLACK;
							gf->_color = RED;
						}
						break;
					}
				}
			}
			_head->_parent->_color = BLACK;
			_head->_left = GetLeft();
			_head->_right = GetRight();
			return true;
		}

		bool IsValidRBTree()
		{
			Node* root = _head->_parent;
			if (!root)
				return true;
			if (root->_color != BLACK)
				return false;
			Node* cur = root;
			int k = 0;
			while (cur)
			{
				if (cur->_color == BLACK)
					k++;
				cur = cur->_left;
			}
			return _IsValidRBTree(root, 0, k);
		}

		void InOrder()
		{
			_InOrder(_head->_parent);
			std::cout << std::endl;
		}

		~RBTree()
		{
			if (_head == nullptr)
				return;
			else
				NodeFree(_head->_parent);
		}
	private:
		void RotateR(Node* gf)
		{
			Node* parent = gf->_left;
			if (gf->_parent == _head)
			{
				_head->_parent = parent;
				parent->_parent = _head;
			}
			else
			{
				if (gf->_parent->_kv.first > parent->_kv.first)
					gf->_parent->_left = parent;
				else
					gf->_parent->_right = parent;

				parent->_parent = gf->_parent;
			}

			gf->_left = parent->_right;
			if (parent->_right)
				parent->_right->_parent = gf;
			parent->_right = gf;
			gf->_parent = parent;
		}

		void RotateL(Node* gf)
		{
			Node* parent = gf->_right;
			if (gf->_parent == _head)
			{
				_head->_parent = parent;
				parent->_parent = _head;
			}
			else
			{
				if (gf->_parent->_kv.first > parent->_kv.first)
					gf->_parent->_left = parent;
				else
					gf->_parent->_right = parent;

				parent->_parent = gf->_parent;
			}

			gf->_right = parent->_left;
			if (parent->_left)
				parent->_left->_parent = gf;
			parent->_left = gf;
			gf->_parent = parent;
		}

		Node* GetLeft()
		{
			Node* parent = _head->_parent;
			Node* cur = parent->_left;
			while (cur)
			{
				parent = cur;
				cur = parent->_left;
			}
			return parent;
		}

		Node* GetRight()
		{
			Node* parent = _head->_parent;
			Node* cur = parent->_right;
			while (cur)
			{
				parent = cur;
				cur = parent->_right;
			}
			return parent;
		}

		void NodeFree(Node* node)
		{
			if (node == nullptr)
				return;
			NodeFree(node->_left);
			NodeFree(node->_right);
			delete node;
		}

		bool _IsValidRBTree(Node* node, int k, int blackcount)
		{
			if (node == nullptr)
			{
				if (k == blackcount)
					return true;
				else
					return false;
			}

			if (node->_parent->_color == RED && node->_color == RED)
				return false;

			if (node->_color == BLACK)
				++k;

			return _IsValidRBTree(node->_left, k, blackcount) && _IsValidRBTree(node->_right, k, blackcount);
		}

		void _InOrder(Node* node)
		{
			if (node == nullptr)
				return;
			_InOrder(node->_left);
			std::cout << node->_kv.second << " ";
			_InOrder(node->_right);
		}

	private:
		Node* _head;
	};

	void TestRBTree1()
	{
		//int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14,8, 3, 1, 10, 6, 4, 7, 14, 13 };
		RBTree<int, int> t1;
		for (auto e : a)
		{
			t1.insert({ e,e });
			std::cout << "Insert:" << e << "->" << t1.IsValidRBTree() << std::endl;
		}

		t1.InOrder();

		std::cout << t1.IsValidRBTree() << std::endl;
	}

	void TestRBTree2()
	{
		const int N = 10000000;
		std::vector<int> v;
		v.reserve(N);
		srand(time(0));

		for (size_t i = 0; i < N; i++)
		{
			v.push_back(rand() + i);
			//cout << v.back() << endl;
		}

		size_t begin2 = clock();
		RBTree<int, int> t;
		for (auto e : v)
		{
			t.insert(std::make_pair(e, e));
			//cout << "Insert:" << e << "->" << t.IsBalance() << endl;
		}
		size_t end2 = clock();

		std::cout << end2 - begin2 << std::endl;

		std::cout << t.IsValidRBTree() << std::endl;
	}

}