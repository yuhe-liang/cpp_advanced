#pragma once
#include <utility>
#include <functional>
#include <iostream>
#include <vector>

namespace my
{
	enum Color
	{
		RED,
		BLACK
	};

	template<class T>
	struct RBTreeNode
	{
		RBTreeNode<T>* _parent;
		RBTreeNode<T>* _left;
		RBTreeNode<T>* _right;
		T _data;
		Color _color;

		RBTreeNode(const T data = T(), Color color = RED)
			:_parent(nullptr)
			,_left(nullptr)
			,_right(nullptr)
			,_data(data)
			,_color(color)
		{}

		RBTreeNode(const RBTreeNode<T>* node)
			:_parent(nullptr)
			, _left(nullptr)
			, _right(nullptr)
			, _data(node->_data)
			, _color(node->_color)
		{}
	};

	template<class T, class Ref, class Ptr>
	struct RBTreeIterator
	{
		typedef RBTreeNode<T> Node;
		typedef RBTreeIterator<T, Ref, Ptr> Self;
		Node* _node;

		RBTreeIterator()
			:_node(nullptr)
		{}

		RBTreeIterator(Node* node)
			:_node(node)
		{}

		Ref operator*()
		{
			return _node->_data;
		}

		Ptr operator->()
		{
			return &_node->_data;
		}

		bool operator!=(const Self& n)
		{
			return _node != n._node;
		}

		bool operator==(const Self& n)
		{
			return !(*this != n);
		}

		Self& operator++()
		{
			if (_node->_right)
			{
				Node* leftMax = _node->_right;
				while (leftMax->_left)
				{
					leftMax = leftMax->_left;
				}
				_node = leftMax;
			}
			else
			{
				Node* cur = _node;
				Node* parent = _node->_parent;
				while (parent && parent->_right == cur)
				{
					cur = parent;
					parent = cur->_parent;
				}
				_node = parent;
			}
			return *this;
		}

		Self& operator--()
		{
			if (_node->_left)
			{
				Node* rightMax = _node->_right;
				while (rightMax->_right)
				{
					rightMax = rightMax->_right;
				}
				_node = rightMax;
			}
			else
			{
				Node* cur = _node;
				Node* parent = _node->_parent;
				while (parent && parent->_right == cur)
				{
					cur = parent;
					parent = cur->_parent;
				}
				_node = cur;
			}
			return *this; 
		}
	};

	template<class K, class T, class Comp, class KeyOfT>
	class RBTree
	{
	public:
		typedef RBTreeNode<T> Node;
		typedef RBTreeIterator<T, T&, T*> Iterator;
		typedef RBTreeIterator<T, const T&, const T*> ConstIterator;
		typedef RBTree<K, T, Comp, KeyOfT> Self;
	
		RBTree()
		{
			_head = new Node(T());
			_size = 0;

		}

		Self& operator=(Self tr)
		{
			_size = tr._size;
			std::swap(_head, tr._head);
			return *this;
		}

		Iterator Begin()
		{
			return Iterator(GetLeft());
		}

		Iterator End()
		{
			return Iterator(_head);
		}

		ConstIterator CBegin() const
		{
			return ConstIterator(GetLeft());
		}

		ConstIterator CEnd() const
		{
			return ConstIterator(nullptr);
		}

		size_t Size()
		{
			return _size;
		}

		std::pair<Iterator, bool> insert(const T& kv)
		{
			KeyOfT key;
			Comp comp;
			Node* root = _head->_parent;
			// 插入
			if (root == nullptr)
			{
				root = new Node(kv);
				root->_parent = _head;
				_head->_parent = _head->_left = _head->_right = root;
				root->_color = BLACK;
				++_size;
				return std::make_pair(Iterator(root), true);
			}

			Node* parent = _head;
			Node* cur = root;
			while (cur)
			{
				if (key(kv) == key(cur->_data))
					return std::make_pair(Iterator(cur), false);
				else if (comp(key(kv), key(cur->_data)))
				{
					parent = cur;
					cur = parent->_left;
				}
				else
				{
					parent = cur;
					cur = parent->_right;
				}
			}

			Node* newNode = new Node(kv);
			cur = newNode;
			if (comp(key(cur->_data), key(parent->_data)))
				parent->_left = cur;
			else
				parent->_right = cur;

			cur->_parent = parent;

			//调整树使其符合规则
			while (parent != _head && parent->_color == RED)
			{
				Node* gf = parent->_parent;
				if (parent == gf->_left)
				{
					Node* uc = gf->_right;
					if (uc && uc->_color == RED)
					{
						parent->_color = BLACK;
						uc->_color = BLACK;
						gf->_color = RED;
						cur = gf;
						parent = gf->_parent;
					}
					else
					{
						if (cur == parent->_left)
						{
							RotateR(gf);
							parent->_color = BLACK;
							gf->_color = RED;
						}
						else
						{
							RotateL(parent);
							RotateR(gf);
							cur->_color = BLACK;
							gf->_color = RED;
						}
						break;
					}
				}
				else
				{
					Node* uc = gf->_left;
					if (uc && uc->_color == RED)
					{
						parent->_color = BLACK;
						uc->_color = BLACK;
						gf->_color = RED;
						cur = gf;
						parent = gf->_parent;
					}
					else
					{
						if (cur == parent->_right)
						{
							RotateL(gf);
							parent->_color = BLACK;
							gf->_color = RED;
						}
						else
						{
							RotateR(parent);
							RotateL(gf);
							cur->_color = BLACK;
							gf->_color = RED;
						}
						break;
					}
				}
			}
			_head->_parent->_color = BLACK;
			_head->_left = GetLeft();
			_head->_right = GetRight();
			++_size;
			return std::make_pair(Iterator(newNode), true);
		}

		Iterator find(const K& k)
		{
			KeyOfT key;
			Comp comp;
			Node* cur = _head->_parent;
			while (cur)
			{
				if (k == key(cur->_data))
				{
					return Iterator(cur);
				}
				else if (comp(k, key(cur->_data)))
				{
					cur = cur->_left;
				}
				else
				{
					cur = cur->_right;
				}
			}
			return Iterator(nullptr);
		}

		bool IsValidRBTree()
		{
			Node* root = _head->_parent;
			if (!root)
				return true;
			if (root->_color != BLACK)
				return false;
			Node* cur = root;
			int k = 0;
			while (cur)
			{
				if (cur->_color == BLACK)
					k++;
				cur = cur->_left;
			}
			return _IsValidRBTree(root, 0, k);
		}

		~RBTree()
		{
			if (_head == nullptr)
				return;
			else
				NodeFree(_head->_parent);
		}
	private:
		void swap(Self& tr)
		{
			std::swap(_head, tr._head);
			std::swap(_size, tr._size);
		}

		void RotateR(Node* gf)
		{
			KeyOfT key;
			Comp comp;
			Node* parent = gf->_left;
			if (gf->_parent == _head)
			{
				_head->_parent = parent;
				parent->_parent = _head;
			}
			else
			{
				if (comp(key(parent->_data), key(gf->_parent->_data)))
					gf->_parent->_left = parent;
				else
					gf->_parent->_right = parent;

				parent->_parent = gf->_parent;
			}

			gf->_left = parent->_right;
			if (parent->_right)
				parent->_right->_parent = gf;
			parent->_right = gf;
			gf->_parent = parent;
		}

		void RotateL(Node* gf)
		{
			KeyOfT key;
			Comp comp;
			Node* parent = gf->_right;
			if (gf->_parent == _head)
			{
				_head->_parent = parent;
				parent->_parent = _head;
			}
			else
			{
				if (comp(key(parent->_data), key(gf->_parent->_data)))
					gf->_parent->_left = parent;
				else
					gf->_parent->_right = parent;

				parent->_parent = gf->_parent;
			}

			gf->_right = parent->_left;
			if (parent->_left)
				parent->_left->_parent = gf;
			parent->_left = gf;
			gf->_parent = parent;
		}

		Node* Copy(const Node* node, const Node* parent = nullptr)
		{
			if (node == nullptr)
				return nullptr;
			Node* cur = new Node(node);
			cur->_parent = parent;
			cur->_left = Copy(node->_left, cur);
			cur->_right = Copy(node->_right, cur);
			return cur;
		}

		Node* GetLeft()
		{
			Node* parent = _head->_parent;
			Node* cur = parent->_left;
			while (cur)
			{
				parent = cur;
				cur = parent->_left;
			}
			return parent;
		}

		Node* GetRight()
		{
			Node* parent = _head->_parent;
			Node* cur = parent->_right;
			while (cur)
			{
				parent = cur;
				cur = parent->_right;
			}
			return parent;
		}

		void NodeFree(Node* node)
		{
			if (node == nullptr)
				return;
			NodeFree(node->_left);
			NodeFree(node->_right);
			delete node;
		}

		bool _IsValidRBTree(Node* node, int k, int blackcount)
		{
			if (node == nullptr)
			{
				if (k == blackcount)
					return true;
				else
					return false;
			}

			if (node->_parent->_color == RED && node->_color == RED)
				return false;

			if (node->_color == BLACK)
				++k;

			return _IsValidRBTree(node->_left, k, blackcount) && _IsValidRBTree(node->_right, k, blackcount);
		}

	private:
		Node* _head;
		size_t _size;
	};
}