#pragma once
#include "RBTree.h"

namespace my
{
	template<class K, class V, class Comp = std::less<K>>
	class map
	{
	public:
		struct MapKeyOfT
		{
			const K& operator()(const std::pair<K, V>& pr)
			{
				return pr.first;
			}
		};
		typedef typename RBTree<K, std::pair<K, V>, Comp, MapKeyOfT>::Iterator iterator;
		typedef typename RBTree<K, std::pair<K, V>, Comp, MapKeyOfT>::ConstIterator const_iterator;

		iterator begin()
		{
			return _tr.Begin();
		}

		iterator cbegin() const
		{
			return _tr.CBegin;
		}

		iterator end()
		{
			return _tr.End();
		}

		iterator cend() const
		{
			return _tr.CEnd();
		}

		iterator find(const K& k)
		{
			return _tr.find(k);
		}

		std::pair<iterator, bool> insert(const K& k)
		{
			return _tr.insert(std::make_pair(k, V()));
		}

		std::pair<iterator, bool> insert(const std::pair<K, V>& pr)
		{
			return _tr.insert(pr);
		}

		V& operator[](const K& k)
		{
			std::pair<iterator, bool> ret = _tr.insert(std::make_pair(k, V()));
			return ret.first->second;
		}
	private:
		RBTree<K, std::pair<K, V>, Comp, MapKeyOfT> _tr;
	};

	void MyMapTest1()
	{
		std::string a[] = { "ƻ��", "�㽶", "��ݮ", "����", "��", "����", "�㽶" , "�㽶", "ƻ��", "�㽶", "��ݮ", "����", "��" };
		map<std::string, int> mp;
		for (auto e : a)
		{
			mp[e]++;
		}
		for (auto e : mp)
		{
			std::cout << e.first << " : " << e.second << std::endl;
		}
		std::cout << std::endl;
	}
}