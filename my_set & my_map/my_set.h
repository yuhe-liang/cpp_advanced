#pragma once
#include "RBTree.h"

namespace my
{
	template<class K, class Comp = std::less<K>>
	class set
	{
	public:
		template<class K>
		struct SetKeyOfT
		{
			const K& operator()(const K& key)
			{
				return key;
			}
		};
		typedef typename RBTree<K, const K, Comp, SetKeyOfT<K>>::Iterator iterator;
		typedef typename RBTree<K, const K, Comp, SetKeyOfT<K>>::ConstIterator const_iterator;

		iterator begin()
		{
			return _tr.Begin();
		}

		iterator cbegin() const
		{
			return _tr.CBegin;
		}
		
		iterator end()
		{
			return _tr.End();
		}

		iterator cend() const
		{
			return _tr.CEnd();
		}

		iterator find(const K& k)
		{
			return _tr.find(k);
		}

		std::pair<iterator, bool> insert(const K& k)
		{
			return _tr.insert(k);
		}

	private:
		RBTree<K, const K, Comp, SetKeyOfT<K>> _tr;
	};

	void SetTest1()
	{
		int arr[] = { 1,3,4,5,64,234,456,123,3445,123,6,43,4,123,4354,123,234,980 };
		set<int> st;
		for (int e : arr)
		{
			st.insert(e);
		}
		for (set<int>::iterator it = st.begin(); it != st.end(); ++it)
		{
			std::cout << *it << ' ';
		}
		std::cout << std::endl;
	}
}