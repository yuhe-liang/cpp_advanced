#pragma once
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

namespace my
{
	template <class T>
	struct HashNode
	{
		HashNode<T>* _next;
		T _data;

		HashNode(const T& data)
			:_next(nullptr), _data(data)
		{}
	};

	template <class K>
	struct HashFunc
	{
		size_t operator()(const K& key)
		{
			return (size_t)key;
		}
	};

	template <>
	struct HashFunc<std::string>
	{
		size_t operator()(const std::string& key)
		{
			size_t hash = 0;
			for (auto ch : key)
			{
				hash *= 131;
				hash += ch;
			}

			return hash;
		}
	};

	template <class K, class T, class KeyOfT, class Hash = HashFunc<K>>
	class HashBucket
	{
		typedef HashNode<T> Node;
	public:
		template <class Ptr, class Ref>
		struct _HIterator
		{
			typedef _HIterator Self;
			typedef HashNode<T> Node;

			_HIterator(Node* node, HashBucket* p)
				:_node(node), _p(p)
			{}

			Node* _node;
			HashBucket* _p;

			Ref operator*()
			{
				return _node->_data;
			}

			Ptr operator->()
			{
				return &_node->_data;
			}

			Self operator++()
			{
				if (_node->_next)
				{
					_node = _node->_next;
				}
				else
				{
					KeyOfT KT;
					Hash Hs;
					K key = KT(_node->_data);
					size_t i = Hs(key) % _p->_v.size() + 1;
					if (i == _p->_v.size())
					{
						_node = nullptr;
						return *this;
					}
					else
					{
						while (!_p->_v[i])
						{ 
							++i;
							if (i == _p->_v.size())
							{
								_node = nullptr;
								return *this;
							}	
						}
						_node = _p->_v[i];
					}
				}
				return *this;
			}

			bool operator!=(const Self& it)
			{
				return _node != it._node;
			}
		};

		typedef _HIterator<T*, T&> iterator;
		typedef _HIterator<const T*, const T&> const_iterator;

		HashBucket()
		{
			_v.resize(10, nullptr);
			_n = 0;
		}

		~HashBucket()
		{
			int n = 0;
			for (int i = 0; i < _v.size(); ++i)
			{
				Node* p = _v[i];
				while (p && n < _n)
				{
					Node* node = p;
					p = p->_next;
					delete node;
					++n;
				}
				if (n == _n)
					break;
			}
		}

		iterator begin()
		{
			for (int i = 0; i < _v.size(); ++i)
			{
				if (_v[i])
					return iterator(_v[i], this);
			}
			return iterator(nullptr, this);
		}

		const_iterator cbegin() const
		{
			for (int i = 0; i < _v.size(); ++i)
			{
				if (_v[i])
					return const_iterator(_v[i], this);
			}
			return const_iterator(nullptr, this);
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}

		const_iterator cend() const
		{
			return const_iterator(nullptr, this);
		}

		iterator find(const K& key)
		{
			KeyOfT kot;
			Hash hs;
			int k = hs(key) % _v.size();
			Node* p = _v[k];
			while (p)
			{
				if (kot(p->_data) == key)
					break;
				p = p->_next;
			}
			return iterator(p, this);
		}

		std::pair<bool, iterator> insert(const T& data)
		{
			KeyOfT kot;
			K key = kot(data);
			iterator it = find(key);
			if (it._node)
				return std::make_pair(false, it);

			Node* node = new Node(data);
			Insert(key, _v, node);
			++_n;
			CheckCapacity(node);
			it = iterator(node, this);

			return std::make_pair(true, it);
		}

		iterator erase(const K& key)
		{
			KeyOfT kot;
			Hash hs;
			int k = hs(key) % _v.size();
			Node* p = _v[k];

			if (p && kot(p->_data) == key)
			{
				_v[k] = p->_next;
				delete p;
				--_n;
				return iterator(_v[k], this);
			}

			Node* prev = p;
			while (p)
			{
				prev = p;
				p = p->_next;
				if (kot(p->_data) == key)
				{
					prev->_next = p->_next;
					delete p;
					p = prev->_next;
					--_n;
					break;
				}
			}

			return iterator(p, this);

		}

		void empty()
		{
			HashBucket<K, T, KeyOfT, Hash> tmp;
			tmp._v.resize(_v.size());
			std::swap(tmp._v, _v);
			std::swap(tmp._n, _n);
		}

		size_t size()
		{
			return _n;
		}

	private:
		void Insert(K& key, std::vector<Node*>& v, Node* node)
		{
			Hash hs;
			int k = hs(key) % v.size();
			node->_next = v[k];
			v[k] = node;
		}

		void CheckCapacity(Node* node)
		{
			int n = 0;
			while (node)
			{
				++n;
				node = node->_next;
			}

			if (n == _v.size())
			{
				std::vector<Node*> v;
				v.resize(n * 2);
				KeyOfT kot;
				
				int num = 0;
				while (num < n)
				{
					if (_v[num])
					{
						Node* p = _v[num];
						while (p)
						{
							K key = kot(p->_data);
							Node* prev = p;
							p = p->_next;
							Insert(key, v, prev);
						}
					}
					++num;
				}
				std::swap(_v, v);
			}
		}

	private:
		std::vector<Node*> _v;
		size_t _n;
	};

	void HashBucketTest1()
	{
		int arr[] = { 123, 1232, 122, 123, 5, 76, 43, 5, 44, 234, 8678, 1, 2 };
		struct KeyOfT
		{
			int& operator()(std::pair<int, int> p) const
			{
				return p.first;
			}
		};

		HashBucket<int, std::pair<int, int>, KeyOfT> hs;

		for (auto& e : arr)
		{
			hs.insert(std::make_pair(e, e));
		}

		std::cout << hs.find(5)->first << std::endl;

		hs.erase(5);
		hs.erase(76);
	}

	void HashBucketTest2()
	{
		int arr[] = { 1, 11, 21, 31, 41, 51, 61, 71, 81, 91, 101, 111, 121 };
		struct KeyOfT
		{
			int& operator()(std::pair<int, int> p) const
			{
				return p.first;
			}
		};

		HashBucket<int, std::pair<int, int>, KeyOfT> hs;

		for (auto& e : arr)
		{
			hs.insert(std::make_pair(e, e));
		}
	}
}