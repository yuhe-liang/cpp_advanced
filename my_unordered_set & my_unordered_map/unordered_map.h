#pragma once
#include "HashBucket.h"
#include<string>

namespace my
{
	template <class K, class V, class Hash = HashFunc<K>>
	class unordered_map
	{
		template <class K, class V>
		struct MapKeyOfT
		{
			const K& operator() (const std::pair<K, V>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename HashBucket<K, std::pair<const K, V>, MapKeyOfT<K, V>, Hash>::iterator iterator;
		typedef typename HashBucket<K, std::pair<const K, V>, MapKeyOfT<K, V>, Hash>::const_iterator const_iterator;
	
		iterator begin()
		{
			return _hb.begin();
		}

		const_iterator cbegin() const
		{
			return _hb.cbegin();
		}

		iterator end()
		{
			return _hb.end();
		}

		const_iterator cend() const
		{
			return _hb.cend();
		}

		V& operator[] (const K& key)
		{
			std::pair<bool, iterator> ret = _hb.insert(std::make_pair(key, V()));
			return ret.second->second;
		}

		iterator find(const K& key)
		{
			return _hb.find(key);
		}

		std::pair<bool, iterator> insert(const std::pair<K, V>& kv)
		{
			return _hb.insert(kv);
		}

		iterator erase(const K& key)
		{
			return _hb.erase(key);
		}

		int size()
		{
			return _hb.size();
		}

		void empty()
		{
			_hb.empty();
		}

	private:
		HashBucket<K, std::pair<const K, V>, MapKeyOfT<K, V>, Hash> _hb;
	};

	void test_unordered_map()
	{
		std::string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����",
	"ƻ��", "�㽶", "ƻ��", "�㽶","ƻ��","��ݮ", "ƻ��","��ݮ" };
		unordered_map<std::string, int> countMap;
		for (auto& e : arr)
		{
			countMap[e]++;
		}

		unordered_map<std::string, int>::iterator it = countMap.begin();
		while (it != countMap.end())
		{
			//it->first += 'x'; // key�����޸�
			it->second += 1;  // value�����޸�
			std::cout << it->first << ":" << it->second << std::endl;
			++it;
		}
		std::cout << std::endl;

		for (auto& kv : countMap)
		{
			std::cout << kv.first << ":" << kv.second << std::endl;
		}
		std::cout << std::endl;
	}
}