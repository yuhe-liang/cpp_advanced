#pragma once
#include "HashBucket.h"

namespace my
{
	template <class K, class Hash = HashFunc<K>>
	class unordered_set
	{
		template <class K>
		struct SetKeyOfT
		{
			const K& operator() (const K& key)
			{
				return key;
			}
		};
	public:
		typedef typename HashBucket<K, const K, SetKeyOfT<K>, Hash>::iterator iterator;
		typedef typename HashBucket<K, const K, SetKeyOfT<K>, Hash>::const_iterator const_iterator;

		iterator begin()
		{
			return _hb.begin();
		}

		const_iterator cbegin() const
		{
			return _hb.cbegin();
		}

		iterator end()
		{
			return _hb.end();
		}

		const_iterator cend() const
		{
			return _hb.cend();
		}

		iterator find(const K& key)
		{
			return _hb.find(key);
		}

		std::pair<bool, iterator> insert(const K& key)
		{
			return _hb.insert(key);
		}

		iterator erase(const K& key)
		{
			return _hb.erase(key);
		}

		int size()
		{
			return _hb.size();
		}

		void empty()
		{
			_hb.empty();
		}
	private:
		HashBucket<K, const K, SetKeyOfT<K>, Hash> _hb;
	};
	void test_unordered_set()
	{
		unordered_set<int> s;
		s.insert(31);
		s.insert(11);
		s.insert(5);
		s.insert(15);
		s.insert(25);

		unordered_set<int>::iterator it = s.begin();
		while (it != s.end())
		{
			//*it = 1;
			std::cout << *it << " ";
			++it;
		}
		std::cout << std::endl;

		for (auto e : s)
		{
			std::cout << e << " ";
		}
		std::cout << std::endl;
	}
}