#pragma once
#include <assert.h>
#include <iostream>
#include <algorithm>
#include <vector>

namespace my
{

	template<class K, class V>
	struct AVLTreeNode
	{
		AVLTreeNode<K, V>* _left;
		AVLTreeNode<K, V>* _right;
		AVLTreeNode<K, V>* _parent;
		std::pair<K, V> _kv;
		int _bf; //balance factor

		AVLTreeNode(const std::pair<K, V>& kv)
			:_left(nullptr)
			, _right(nullptr)
			, _parent(nullptr)
			, _kv(kv)
			, _bf(0)
		{}
	};

	template<class K, class V>
	class AVLTree
	{
		typedef AVLTreeNode<K, V> Node;
	public:
		AVLTree()
			:_root(nullptr)
		{}

		bool insert(std::pair<K, V> kv)
		{
			// 插入
			if (_root == nullptr)
			{
				_root = new Node(kv);
				return true;
			}

			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_kv.first > kv.first)
				{
					parent = cur;
					cur = parent->_left;
				}
				else if (cur->_kv.first < kv.first)
				{
					parent = cur;
					cur = parent->_right;
				}
				else
					return false;
			}

			cur = new Node(kv);
			if (parent->_kv.first > kv.first)
				parent->_left = cur;
			else
				parent->_right = cur;

			cur->_parent = parent;

			// 平衡
			while (parent)
			{
				if (parent->_left == cur)
					parent->_bf--;
				else
					parent->_bf++;

				if (parent->_bf == 0)
					break;
				else if (parent->_bf == 1 || parent->_bf == -1)
				{
					cur = parent;
					parent = cur->_parent;
				}
				else if (parent->_bf == -2)
				{
					if (cur->_bf == -1)
						parent = RotateR(parent);
					else if (cur->_bf == 1)
						parent = RotateLR(parent);
					else
						assert(false);

					break;
				}
				else if (parent->_bf == 2)
				{
					if (cur->_bf == 1)
						parent = RotateL(parent);
					else if (cur->_bf == -1)
						parent = RotateRL(parent);
					else
						assert(false);

					break;
				}
				else
					assert(false);
			}
			return true;
		}

		bool IsBalance()
		{
			return _IsBalance(_root);
		}

		void InOrder()
		{
			_InOrder(_root);
			std::cout << std::endl;
		}

		size_t size()
		{
			return _size(_root);
		}

		size_t height()
		{
			return _height(_root);
		}

		Node* find(const K& k)
		{
			Node* cur = _root;
			while (cur)
			{
				if (cur->_kv.first == k)
					return cur;
				else if (cur->_kv.first > k)
					cur = cur->_left;
				else
					cur = cur->_right;
			}
			return nullptr;
		}

	private:
		Node* RotateR(Node* parent)
		{
			Node* sub = parent->_left;
			Node* subR = sub->_right;

			if (parent == _root)
			{
				_root = sub;
				sub->_parent = nullptr;
			}
			else
			{
				Node* up = parent->_parent;
				if (up->_kv.first > sub->_kv.first)
					up->_left = sub;
				else
					up->_right = sub;

				sub->_parent = up;
			}

			parent->_left = subR;
			if (subR != nullptr)
				subR->_parent = parent;
			sub->_right = parent;
			parent->_parent = sub;

			parent->_bf = 0;
			sub->_bf = 0;
			return sub;
		}

		Node* RotateLR(Node* parent)
		{
			Node* child = parent->_left;
			Node* sub = child->_right;
			Node* subL = sub->_left;
			Node* subR = sub->_right;

			if (parent == _root)
			{
				_root = sub;
				sub->_parent = nullptr;
			}
			else
			{
				Node* up = parent->_parent;
				if (up->_kv.first > sub->_kv.first)
					up->_left = sub;
				else
					up->_right = sub;

				sub->_parent = up;
			}

			sub->_right = parent;
			parent->_parent = sub;
			sub->_left = child;
			child->_parent = sub;
			parent->_left = subR;
			if (subR != nullptr)
				subR->_parent = parent;
			child->_right = subL;
			if (subL != nullptr)
				subL->_parent = child;

			if (sub->_bf == 1)
			{
				parent->_bf = 0;
				child->_bf = -1;
			}
			else if (sub->_bf == -1)
			{
				parent->_bf = 1;
				child->_bf = 0;
			}
			else
			{
				parent->_bf = 0;
				child->_bf = 0;
			}
			sub->_bf = 0;
			return sub;
		}

		Node* RotateL(Node* parent)
		{
			Node* sub = parent->_right;
			Node* subL = sub->_left;

			if (parent == _root)
			{
				_root = sub;
				sub->_parent = nullptr;				
			}
			else
			{
				Node* up = parent->_parent;
				if (up->_kv.first > sub->_kv.first)
					up->_left = sub;
				else
					up->_right = sub;

				sub->_parent = up;
			}

			parent->_right = subL;
			if (subL != nullptr)
				subL->_parent = parent;
			sub->_left = parent;
			parent->_parent = sub;

			parent->_bf = 0;
			sub->_bf = 0;
			return sub;
		}

		Node* RotateRL(Node* parent)
		{
			Node* child = parent->_right;
			Node* sub = child->_left;
			Node* subL = sub->_left;
			Node* subR = sub->_right;

			if (parent == _root)
			{
				_root = sub;
				sub->_parent = nullptr;
			}
			else
			{
				Node* up = parent->_parent;
				if (up->_kv.first > sub->_kv.first)
					up->_left = sub;
				else
					up->_right = sub;

				sub->_parent = up;
			}

			sub->_left = parent;
			parent->_parent = sub;
			sub->_right = child;
			child->_parent = sub;
			parent->_right = subL;
			if (subL != nullptr)
				subL->_parent = parent;
			child->_left = subR;
			if (subR != nullptr)
				subR->_parent = child;

			if (sub->_bf == 1)
			{
				parent->_bf = -1;
				child->_bf = 0;
			}
			else if (sub->_bf == -1)
			{
				parent->_bf = 0;
				child->_bf = 1;
			}
			else
			{
				parent->_bf = 0;
				child->_bf = 0;
			}
			sub->_bf = 0;
			return sub;
		}

		bool _IsBalance(Node* node)
		{
			if (node == nullptr)
				return true;

			if (node->_bf < -1 || node->_bf > 1)
				return false;

			return _IsBalance(node->_left) && _IsBalance(node->_right);
		}

		void _InOrder(Node* node)
		{
			if (node == nullptr)
				return;

			_InOrder(node->_left);
			std::cout << node->_kv.second << " ";
			_InOrder(node->_right);
		}

		size_t _size(Node* node)
		{
			if (node == nullptr)
				return 0;
			return _size(node->_left) + _size(node->_right) + 1;
		}

		size_t _height(Node* node)
		{
			if (node == nullptr)
				return 0;
			return std::max(_height(node->_left), _height(node->_right)) + 1;
		}

	private:
		Node* _root;
	};

	void test1()
	{
		//int a[] = { 3, 2, 1 };
		int a[] = { 3,1,2 };
		AVLTree<int, int> tr1;
		for (auto e : a)
		{
			tr1.insert({ e, e });
		}
	}

	void test2()
	{
		int a[] = { 1, 2, 3 };
		AVLTree<int, int> tr1;
		for (auto e : a)
		{
			tr1.insert({ e, e });
		}
	}

	void test3()
	{
		//int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		//int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
		int a[] = {16, 3, 7, 11, 9, 26, 18, 14, 15};
		AVLTree<int, int> tr1;
		for (auto e : a)
		{
			tr1.insert({ e, e });
			std::cout << "Insert:" << e << "->" << tr1.IsBalance() << std::endl;
		}

		tr1.InOrder();

		std::cout << tr1.IsBalance() << std::endl;
	}

	void test4()
	{
		const int N = 1000000;
		std::vector<int> v;
		v.reserve(N);
		srand(time(0));

		for (size_t i = 0; i < N; i++)
		{
			v.push_back(rand() + i);
			//cout << v.back() << endl;
		}

		size_t begin2 = clock();
		AVLTree<int, int> t;
		for (auto e : v)
		{
			t.insert({ e,e });
			//cout << "Insert:" << e << "->" << t.IsBalance() << endl;
		}
		size_t end2 = clock();

		std::cout << "Insert:" << end2 - begin2 << std::endl;
		//cout << t.IsBalance() << endl;

		std::cout << "Height:" << t.height() << std::endl;
		std::cout << "Size:" << t.size() << std::endl;

		size_t begin1 = clock();
		// 确定在的值
		for (auto e : v)
		{
			t.find(e);
		}

		// 随机值
		/*for (size_t i = 0; i < N; i++)
		{
			t.Find((rand() + i));
		}*/

		size_t end1 = clock();

		std::cout << "Find:" << end1 - begin1 << std::endl;
	}
}